package de.colo.fin.imports;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.FileReader;


public class ImportCsv {

    private static final Logger LOGGER = LogManager.getLogger(ImportCsv.class);

    public static void main(String[] args) {
        try {
            BasicConfigurator.configure();
            ImportCsv importCsv = new ImportCsv();
            String importFilePath = importCsv.getImportFile("test.CSV");

            if (StringUtils.isEmpty(importFilePath)){
                LOGGER.info("Keine Datei mit diesem Namen gefunden");
                return;
            }

            CSVParser parser = new CSVParser(
                    new FileReader(importFilePath),
                    CSVFormat.DEFAULT.withHeader()
                            .withQuote(null)
                            .withDelimiter(';'));
            for (CSVRecord record : parser) {
                String account = record.get("\"Auftragskonto\"");
                String bookingDate = record.get("\"Buchungstag\"");
                String paymentDate = record.get("\"Valutadatum\"");
                String bookingText = record.get("\"Buchungstext\"");
                String usageText = record.get("\"Verwendungszweck\"");
                String creditorId = record.get("\"Glaeubiger ID\"");
                String mandateReference = record.get("\"Mandatsreferenz\"");
                String customerReference = record.get("\"Kundenreferenz (End-to-End)\"");
                String collectReference = record.get("\"Sammlerreferenz\"");
                String debitSourceSum = record.get("\"Lastschrift Ursprungsbetrag\"");
                String returnDebit = record.get("\"Auslagenersatz Ruecklastschrift\"");
                String receiverName = record.get("\"Beguenstigter/Zahlungspflichtiger\"");
                String receiverAccountNumber = record.get("\"Kontonummer/IBAN\"");
                String receiverBic = record.get("\"BIC (SWIFT-Code)\"");
                String sum = record.get("\"Betrag\"");
                String currency = record.get("\"Waehrung\"");
                String info = record.get("\"Info\"");

            }
            parser.close();
        } catch (final Exception e) {
            LOGGER.error(e);
            System.out.println(e.getMessage());
        }
    }

    private String getImportFile(final String fileName) {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            return classLoader.getResource(fileName).getPath();
        } catch (Exception e) {
            LOGGER.error(e);
            return StringUtils.EMPTY;
        }

    }

}