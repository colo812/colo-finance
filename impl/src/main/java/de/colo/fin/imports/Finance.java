package de.colo.fin.imports;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by pauwed on 19.02.2018.
 */
public class Finance {
    String auftragskonto;
    Date bookingDate;
    Date paymentDate;
    String bookingText;
    String usageText;
    String creditorId;
    String mandateReference;
    String customerReference;
    String collectReference;
    String debitSourceSum;
    String returnDebit;
    String receiverName;
    String receiverAccountNumber;
    String receiverBic;
    BigDecimal sum;
    String currency ;
    String info;
}
