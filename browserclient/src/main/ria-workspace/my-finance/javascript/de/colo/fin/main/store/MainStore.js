Ext.define('de.colo.fin.main.store.MainStore', {
    extend: 'Ext.data.Store',
    model: 'de.colo.fin.main.MainModel',
    proxy: {
            type: 'ajax',
            url : 'users.json'
        },
        autoLoad: true
});