Ext.define('de.colo.fin.main.controller.MainViewController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    control: {
        '[reference=save]': {
            click: 'handleClickText'
        }
    },

    handleClickText: function () {
        var me = this,
        view = me.getView(),
        grid = view.down('grid'),
        store = grid.getStore(),
        firstNameField = view.down('[reference=firstName]'),
        lastNameField = view.down('[reference=lastName]'),
        ageField = view.down('[reference=age]'),
        firstName = firstNameField.getValue(),
        lastName = lastNameField.getValue(),
        age = ageField.getValue();

        store.add(
            {
                firstName: firstName,
                lastName: lastName,
                age: age
            }
        );


    }

});