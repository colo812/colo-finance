Ext.define('de.colo.fin.main.view.Main', {
    extend: 'Ext.container.Container',
    xtype: 'app-main',
    requires: [
        'de.colo.fin.main.controller.MainViewController'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    controller: 'main',

    items: [
        {
            layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
            items: [
                {
                    xtype: 'textfield',
                    reference: 'firstName',
                    fieldLabel: 'First Name'
                },
                {
                    xtype: 'textfield',
                    reference: 'lastName',
                    fieldLabel: 'Last Name'
                },
                {
                    xtype: 'numberfield',
                    reference: 'age',
                    fieldLabel: 'Age'
                },
                {
                    xtype: 'button',
                    reference: 'save',
                    text: 'Speichern'
                }
            ]

        },
        {
            xtype: 'grid',
            store: Ext.create('de.colo.fin.main.store.MainStore'),
            columns: [
                {
                     text: 'First Name',
                     dataIndex: 'firstName',
                     flex: 1
                },
                {
                     text: 'Last Name',
                     dataIndex: 'lastName',
                     flex: 1
                },
                {
                     text: 'Age',
                     dataIndex: 'age',
                     flex: 1
                }
            ]
        }
    ]
});
