Ext.define('de.colo.fin.main.MainModel', {
    extend: 'Ext.data.Model',
    alias: 'model.mainmodel',
    fields: [
        {name: 'firstName',  type: 'string'},
        {name: 'lastName',  type: 'string'},
        {name: 'age',   type: 'int'}
    ]
});